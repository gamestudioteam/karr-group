﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameUI : MonoBehaviour {
	public Sprite HP10;
	public Sprite HP20;
	public Sprite HP30;
	public Sprite HP40;
	public Sprite HP50;
	public Sprite HP60;
	public Sprite HP70;
	public Sprite HP80;
	public Sprite HP90;
	public Sprite HP100;
	public Image HpBar;
	public float HP;
	public int lives;
	public Sprite GotALife;
	public GameObject Life1;
	public GameObject Life2;
	public GameObject Life3;
	public Sprite SpecialBuilding;
	public Sprite SpecialFlash1;
	public Sprite SpecialFlash2;
	public Image Special;

	private GameObject playerController;
	private PlayerControllerScript pcs;
	

	// Use this for initialization
	void Start () {

		if (this.tag == "HUD 1") 
		{
			playerController = GameObject.FindGameObjectWithTag ("Controller 1");

		}
		else if (this.tag == "HUD 2")  {
			playerController = GameObject.FindGameObjectWithTag ("Controller 2");
		}
		else if (this.tag == "HUD 3")  {
			playerController = GameObject.FindGameObjectWithTag ("Controller 3");

		}
		else if (this.tag == "HUD 4") {
			playerController = GameObject.FindGameObjectWithTag ("Controller 4");
		}

		pcs = playerController.gameObject.GetComponent<PlayerControllerScript> ();

	}
	
	// Update is called once per frame
	void Update () {

		HP = pcs.currentHp;

		if (HP == 100) {
			HpBar.sprite=HP100;
		}
		if (HP < 100) {
			HpBar.sprite=HP90;
		}
		if (HP < 90) {
			HpBar.sprite=HP80;
		}
		if (HP < 80) {
			HpBar.sprite=HP70;
}
		if (HP < 70) {
			HpBar.sprite=HP60;
		}
		if (HP < 60) {
			HpBar.sprite=HP50;
		}
		if (HP < 50) {
			HpBar.sprite=HP40;
		}
		if (HP < 40) {
			HpBar.sprite=HP30;
		}	
		if (HP < 30) {
			HpBar.sprite=HP20;
		}
		if (HP < 20) {
			HpBar.sprite=HP10;
		}

		lives = pcs.lives;

		if (lives == 2) {
			Life1.SetActive(false);
		} 
		else if (lives == 1) {
			Life2.SetActive(false);
			Life1.SetActive(false);
		} 
		else if (lives == 0) {
			Life3.SetActive(false);
			Life2.SetActive(false);
			Life1.SetActive(false);
		}

	}
}
		