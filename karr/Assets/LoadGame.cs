﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadGame : MonoBehaviour {

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Alpha1)) {
			Application.LoadLevel(2);
		}
		if (Input.GetKey (KeyCode.Alpha2)) {
			Application.LoadLevel(3);
		}
		if (Input.GetKey (KeyCode.Alpha3)) {
			Application.LoadLevel(4);
		}
		if (Input.GetKey (KeyCode.Alpha4)) {
			Application.LoadLevel(5);
		}
	}
}
