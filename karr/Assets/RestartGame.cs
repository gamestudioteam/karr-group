﻿using UnityEngine;
using System.Collections;

public class RestartGame : MonoBehaviour {

	private GameObject control1;
	private GameObject control2;
	private GameObject control3;
	private GameObject control4;

	private GameObject controlg;

	private bool ready;

	// Use this for initialization
	void Start () {
		ready = false;

		control1 = GameObject.FindWithTag ("Controller 1");
		control2 = GameObject.FindWithTag ("Controller 2");
		control3 = GameObject.FindWithTag ("Controller 3");
		control4 = GameObject.FindWithTag ("Controller 4");

		controlg = GameObject.FindWithTag ("Game Controller");

		Invoke ("SetReady", 4);

	}

	// Update is called once per frame
	void Update () {
		ready = false;

		if (Input.GetKey (KeyCode.Space)) {
			if (ready = true) {
				Destroy (control1);
				Destroy (control2);
				Destroy (control3);
				Destroy (control4);
				Destroy (controlg);
				Application.LoadLevel (0);
			}
		}
	}
	

	void SetReady() {
		ready = true;
	}

}
	