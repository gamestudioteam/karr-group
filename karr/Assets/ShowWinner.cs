﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowWinner : MonoBehaviour {

	GameObject gC;
	GameController gameController;

	Text winner;
	// Use this for initialization
	void Start () {
	
		gC = GameObject.FindGameObjectWithTag ("Game Controller");
		gameController = gC.GetComponent<GameController> ();

		winner = GetComponent<Text>();

		winner.text = "Player " + gameController.winner + " is the Ace of Karrs";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
