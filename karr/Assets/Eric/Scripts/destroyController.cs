﻿using UnityEngine;
using System.Collections;

public class destroyController : MonoBehaviour {

	//arch pieces
	public GameObject topLeft;
	public GameObject topRight;
	public GameObject legLeft;
	public GameObject legRight;

	//the arch fracture objects
	public GameObject fractureTopLeft;
	public GameObject fractureTopRight;
	public GameObject fractureLegLeft;
	public GameObject fractureLegRight;

	//leg chunks objects
	public GameObject chunkWedge1;
	public GameObject chunkWedge2;
	public GameObject chunkWedge3;
	public GameObject chunkAngle1;
	public GameObject chunkAngle2;
	public GameObject chunkAngle3;
	public GameObject chunkCube;

	//leg chunks objects
	public GameObject piecesWedge;
	public GameObject piecesAngle;
	public GameObject piecesCube;

	void Start() {
		if (this.gameObject == legLeft || this.gameObject == legRight || this.gameObject == topLeft || this.gameObject == topRight) {
			this.GetComponent<Rigidbody> ().useGravity = false;
		} else {
			this.GetComponent<Rigidbody> ().useGravity = true;
		}
	}

	void checkForGravity(Collision coll) {
		if (this.gameObject == legLeft) {
			Instantiate (fractureLegLeft, transform.position, transform.rotation);
		}
		if (this.gameObject == legRight) {
			Instantiate (fractureLegRight, transform.position, transform.rotation);
		}
		if (this.gameObject == topRight) {
			Instantiate (fractureTopRight, transform.position, transform.rotation);
		}
		if (this.gameObject == topLeft) {
			Instantiate (fractureTopLeft, transform.position, transform.rotation);
		}
		if (topLeft != null && topLeft != coll.gameObject) {
			if (topLeft.GetComponent<Rigidbody>().useGravity == false) {
				topLeft.GetComponent<Rigidbody>().useGravity = true;
			}
		}
		if (topRight != null && topRight != coll.gameObject) {
			if (topRight.GetComponent<Rigidbody>().useGravity == false) {
				topRight.GetComponent<Rigidbody>().useGravity = true;
			}
		}
		if (legRight != null && legRight != coll.gameObject) {
			if (legRight.GetComponent<Rigidbody>().useGravity == false) {
				legRight.GetComponent<Rigidbody>().useGravity = true;
			}
		}
		if (legLeft != null && legLeft != coll.gameObject) {
			if (legLeft.GetComponent<Rigidbody>().useGravity == false) {
				legLeft.GetComponent<Rigidbody>().useGravity = true;
			}
		}
		if (this.gameObject == legLeft || this.gameObject == legRight || this.gameObject == topLeft || this.gameObject == topRight) {
			Destroy (gameObject);
		}
	}
	void OnCollisionEnter(Collision other) {
		if (other.gameObject.tag == "sniper") {
			checkForGravity (other);
			if (this.gameObject == chunkWedge1 || this.gameObject == chunkWedge2 || this.gameObject == chunkWedge3) {
				Instantiate (piecesWedge, transform.position, transform.rotation);
				Destroy (gameObject);
			}
			if (this.gameObject == chunkAngle1 || this.gameObject == chunkAngle2 ||this.gameObject == chunkAngle3) {
				Instantiate (piecesAngle, transform.position, transform.rotation);
				Destroy (gameObject);
			}
			if (this.gameObject == chunkCube) {
				Instantiate (piecesCube, transform.position, transform.rotation);
				Destroy (gameObject);
			}
		}
	}


//		//left leg
//			if (this.gameObject == legLeft) {
//				Instantiate (fractureLegLeft, transform.position, transform.rotation);
//				if (topLeft != null) {
//					if (topLeft.GetComponent<Rigidbody>().useGravity == false) {
//						topLeft.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				if (topRight != null) {
//					if (topRight.GetComponent<Rigidbody>().useGravity == false) {
//						topRight.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				if (legRight != null) {
//					if (legRight.GetComponent<Rigidbody>().useGravity == false) {
//						legRight.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				Destroy (gameObject);
//			}
//		}
//		//right leg
//		if (other.gameObject.tag == "Player") {
//			if (this.gameObject == legRight) {
//				Instantiate (fractureLegLeft, transform.position, transform.rotation);
//				if (topLeft != null) {
//					if (topLeft.GetComponent<Rigidbody>().useGravity == false) {
//						topLeft.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				if (topRight != null) {
//					if (topRight.GetComponent<Rigidbody>().useGravity == false) {
//						topRight.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				if (legLeft != null) {
//					if (legLeft.GetComponent<Rigidbody>().useGravity == false) {
//						legLeft.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				Destroy (gameObject);
//			}
//		}
//		//top left
//		if (other.gameObject.tag == "Player") {
//			if (this.gameObject == topLeft) {
//				Instantiate (fractureTopLeft, transform.position, transform.rotation);
//				if (legLeft != null) {
//					if (legLeft.GetComponent<Rigidbody>().useGravity == false) {
//						legLeft.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				if (topRight != null) {
//					if (topRight.GetComponent<Rigidbody>().useGravity == false) {
//						topRight.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				if (legRight != null) {
//					if (legLeft.GetComponent<Rigidbody>().useGravity == false) {
//						legRight.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				Destroy (gameObject);
//			}
//		}
//		//top right
//		if (other.gameObject.tag == "Player") {
//			if (this.gameObject == topRight) {
//				Instantiate (fractureTopRight, transform.position, transform.rotation);
//				if (topLeft != null) {
//					if (topLeft.GetComponent<Rigidbody>().useGravity == false) {
//						topLeft.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				if (legLeft != null) {
//					if (legLeft.GetComponent<Rigidbody>().useGravity == false) {
//						legLeft.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				if (legRight != null) {
//					if (legRight.GetComponent<Rigidbody>().useGravity == false) {
//						legRight.GetComponent<Rigidbody>().useGravity = true;
//					}
//				}
//				Destroy (gameObject);
//			}
//		}

}


	
