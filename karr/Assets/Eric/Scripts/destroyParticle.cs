﻿using UnityEngine;
using System.Collections;

public class destroyParticle : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (gameObject.tag == "explosionParticle") {
			Destroy (gameObject, 1.5f);
		}
	}

}