﻿using UnityEngine;
using System.Collections;

public class centerDestroy : MonoBehaviour {
		
		//center Arch
		public GameObject centerLeg1;
		public GameObject centerLeg2;
		public GameObject centerLeg3;
		public GameObject centerLeg4;
		
		public GameObject centerTop1;
		public GameObject centerTop2;
		public GameObject centerTop3;
		public GameObject centerTop4;
		
		//center Arch fractures
		public GameObject centerLegFracture1;
		public GameObject centerLegFracture2;
		public GameObject centerLegFracture3;
		public GameObject centerLegFracture4;
		
		public GameObject centerTopFracture1;
		public GameObject centerTopFracture2;
		public GameObject centerTopFracture3;
		public GameObject centerTopFracture4;
		
		//center Arch chunks
		public GameObject centerChunkWedge1;
		public GameObject centerChunkWedge2;
		public GameObject centerChunkWedge3;
		public GameObject centerChunkAngle1;
		public GameObject centerChunkAngle2;
		public GameObject centerChunkAngle3;
		public GameObject centerChunkCube;

		//leg chunks objects
		public GameObject centerPiecesWedge;
		public GameObject centerPiecesAngle;
		public GameObject centerPiecesCube;
		
		void Start() {
		if (this.gameObject == centerLeg1 || this.gameObject == centerLeg2 || this.gameObject == centerLeg3 || this.gameObject == centerLeg4 || this.gameObject == centerTop1 || this.gameObject == centerTop2 || this.gameObject == centerTop3 || this.gameObject == centerTop4) {
				this.GetComponent<Rigidbody> ().useGravity = false;
			} else {
				this.GetComponent<Rigidbody> ().useGravity = true;
			}
		}
		
		void checkForGravity(Collision coll) {
			if (this.gameObject == centerLeg1) {
				Instantiate (centerLegFracture1, transform.position, transform.rotation);
			}
			if (this.gameObject == centerLeg2) {
				Instantiate (centerLegFracture2, transform.position, transform.rotation);
			}
			if (this.gameObject == centerLeg3) {
				Instantiate (centerLegFracture3, transform.position, transform.rotation);
			}
			if (this.gameObject == centerLeg4) {
				Instantiate (centerLegFracture4, transform.position, transform.rotation);
			}
			if (this.gameObject == centerTop1) {
				Instantiate (centerTopFracture1, transform.position, transform.rotation);
			}
			if (this.gameObject == centerTop2) {
				Instantiate (centerTopFracture2, transform.position, transform.rotation);
			}
			if (this.gameObject == centerTop3) {
				Instantiate (centerTopFracture3, transform.position, transform.rotation);
			}
			if (this.gameObject == centerTop4) {
				Instantiate (centerTopFracture4, transform.position, transform.rotation);
			}
			if (centerLeg1 != null && centerLeg1 != coll.gameObject) {
				if (centerLeg1.GetComponent<Rigidbody>().useGravity == false) {
					centerLeg1.GetComponent<Rigidbody>().useGravity = true;
				}
			}
			if (centerLeg2 != null && centerLeg2 != coll.gameObject) {
				if (centerLeg2.GetComponent<Rigidbody>().useGravity == false) {
					centerLeg2.GetComponent<Rigidbody>().useGravity = true;
				}
			}
			if (centerLeg3 != null && centerLeg3 != coll.gameObject) {
				if (centerLeg3.GetComponent<Rigidbody>().useGravity == false) {
					centerLeg3.GetComponent<Rigidbody>().useGravity = true;
				}
			}
			if (centerLeg4 != null && centerLeg4 != coll.gameObject) {
				if (centerLeg4.GetComponent<Rigidbody>().useGravity == false) {
					centerLeg4.GetComponent<Rigidbody>().useGravity = true;
				}
			}
			if (centerTop1 != null && centerLeg1 != coll.gameObject) {
				if (centerTop1.GetComponent<Rigidbody>().useGravity == false) {
					centerTop1.GetComponent<Rigidbody>().useGravity = true;
				}
			}
			if (centerTop2 != null && centerLeg2 != coll.gameObject) {
				if (centerTop2.GetComponent<Rigidbody>().useGravity == false) {
					centerTop2.GetComponent<Rigidbody>().useGravity = true;
				}
			}
			if (centerTop3 != null && centerLeg3 != coll.gameObject) {
				if (centerTop3.GetComponent<Rigidbody>().useGravity == false) {
					centerTop3.GetComponent<Rigidbody>().useGravity = true;
				}
			}
			if (centerTop4 != null && centerLeg4 != coll.gameObject) {
				if (centerTop4.GetComponent<Rigidbody>().useGravity == false) {
					centerTop4.GetComponent<Rigidbody>().useGravity = true;
				}
			}
			if (this.gameObject == centerLeg1 || this.gameObject == centerLeg2 || this.gameObject == centerLeg3 || this.gameObject == centerLeg4 || this.gameObject == centerTop1 || this.gameObject == centerTop2 || this.gameObject == centerTop3 || this.gameObject == centerTop4) {
					Destroy (gameObject);
				}
			}
		void OnCollisionEnter(Collision other) {
			if (other.gameObject.tag == "sniper") {
				checkForGravity (other);
			if (this.gameObject == centerChunkWedge1 || this.gameObject == centerChunkWedge2 || this.gameObject == centerChunkWedge3) {
					Instantiate (centerPiecesWedge, transform.position, transform.rotation);
					Destroy (gameObject);
				}
			if (this.gameObject == centerChunkAngle1 || this.gameObject == centerChunkAngle2 ||this.gameObject == centerChunkAngle3) {
					Instantiate (centerPiecesAngle, transform.position, transform.rotation);
					Destroy (gameObject);
				}
			if (this.gameObject == centerChunkCube) {
					Instantiate (centerPiecesCube, transform.position, transform.rotation);
					Destroy (gameObject);
				}
			}
		}
	}
	
	
	
