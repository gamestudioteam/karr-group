﻿using UnityEngine;
using System.Collections;

public class respawnCar : MonoBehaviour {

	public bool isFree = true;

	void update () {

	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == "Player 1" || other.gameObject.tag == "Player 2" || other.gameObject.tag == "Player 3" || other.gameObject.tag == "Player 4" || other.gameObject.tag == "destructable") {
			isFree = false;		
		}
	}

	void OnTriggerStay (Collider other) {
		if (other.gameObject.tag == "Player 1" || other.gameObject.tag == "Player 2" || other.gameObject.tag == "Player 3" || other.gameObject.tag == "Player 4" || other.gameObject.tag == "destructable") {
			isFree = false;
		}
	}

	void OnTriggerExit (Collider other) {
		if (other.gameObject.tag == "Player 1" || other.gameObject.tag == "Player 2" || other.gameObject.tag == "Player 3" || other.gameObject.tag == "Player 4" || other.gameObject.tag == "destructable") {
			isFree = true;
		}
	}
}
