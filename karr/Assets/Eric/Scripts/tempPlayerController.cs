﻿using UnityEngine;
using System.Collections;

public class tempPlayerController : MonoBehaviour {

// this bool tells us if car is dead or not
	public bool isCarActive = true;

	public GameObject currentPlayer;
	private int currentSpawnTimer;
	public GameObject tempGameController;

	void setNewSpawnTimer () {
		currentSpawnTimer = 5;
		InvokeRepeating ("countdownSpawnTimer", 1f, 1f);
	}

	void countdownSpawnTimer () {
		currentSpawnTimer--;
		print (currentSpawnTimer);
		if (currentSpawnTimer == 0) {
			CancelInvoke ("countdownSpawnTimer");
//			print ("Invoke Cancelled");
			tempGameController.GetComponent<tempGameController>().SendMessage ("respawnPlayer", currentPlayer);
//			print ("sent to respawnPlayer");
		}
	}

	void Start () {
		setNewSpawnTimer ();
	}
}
