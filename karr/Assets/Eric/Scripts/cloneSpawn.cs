﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class cloneSpawn : MonoBehaviour {
	
	private bool cloneSpecialAvailable = true;
	
	public GameObject sniperClone;
	
	public List<GameObject> spawnPoints;
	
	private int currentCloneTimer;

	void Start () {
		spawnPoints.Add(GameObject.FindGameObjectWithTag("Respawn 1"));
		spawnPoints.Add(GameObject.FindGameObjectWithTag("Respawn 2"));
		spawnPoints.Add(GameObject.FindGameObjectWithTag("Respawn 3"));
		spawnPoints.Add(GameObject.FindGameObjectWithTag("Respawn 4"));
	}

	void setNewCloneTimer () {
		currentCloneTimer = 5;
		InvokeRepeating ("countdownCloneTimer", 1f, 1f);
	}
	
	void countdownCloneTimer () {
		currentCloneTimer--;
		print (currentCloneTimer);
		if (currentCloneTimer == 0) {
			CancelInvoke ("countdownCloneTimer");
			//            print ("Invoke Cancelled");
			cloneSpecialAvailable = true;
			//            print ("Special reset");
		}
	}

	void translatePlayer (GameObject sniperPlayer) {
		GameObject randomSpawnPoint = spawnPoints[Random.Range (0, spawnPoints.Count)];
		print (randomSpawnPoint);
		if (randomSpawnPoint.GetComponent<respawnCar>().isFree == true) {
		//	translate current real sniper player to a spawnpoint
						Vector3 randomSpawnPointPosition = randomSpawnPoint.transform.position;
		//				Vector3 randomSpawnPointRotation = Quaternion.Euler (randomSpawnPoint.transform.rotation);
						this.transform.position = randomSpawnPointPosition;
		//				Rigidbody.rotation = randomSpawnPointRotation;
		} else {
			translatePlayer(this.gameObject);
		}
	}

	void clonePlayer () {
		//get current speed / direction
		Vector3 add = new Vector3 (0, 0, 6);
		Instantiate (sniperClone, this.transform.position + add, this.transform.rotation);
		sniperClone.GetComponent<Rigidbody>().isKinematic = true;
		sniperClone.gameObject.tag = "clone";
		
		//get player speed / direction
//		CharacterController sniperController = GetComponent<CharacterController>();
//		Vector3 playerHorizontalVelocity = sniperController.velocity;
//		playerHorizontalVelocity = new Vector3(sniperController.velocity.x, 0, sniperController.velocity.z);

		//		//set clone to player speed / direction
		//		CharacterController cloneController = sniperClone.gameObject.GetComponent<CharacterController>();
		//		Vector3 cloneHorizontalVelocity = cloneController.velocity;
		//		cloneHorizontalVelocity = new Vector3(cloneController.velocity.x, 0, cloneController.velocity.z);

		//		//set clone to player speed / direction
		//		cloneHorizontalVelocity = playerHorizontalVelocity;
		
		translatePlayer (this.gameObject);
		cloneSpecialAvailable = false;
		setNewCloneTimer();
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
			print ("got jump");
			if (cloneSpecialAvailable == true) {
				clonePlayer ();
			} 
		}
	}
}

//function Update () { if (Input.GetKeyDown("e") /&& player is in tank/) 
	// commented out for your code to replace { tank = GameObject.Find("Tank"); 
	// finds the GameObject named if ( tank ) { player.position = (tank.position + (2,0,0)); player.rotation = tank.rotation; 
	// make the player visible here } } }