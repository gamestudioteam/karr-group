﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class tempGameController : MonoBehaviour {
	
	public List<GameObject> spawnPoints;


	void respawnPlayer (GameObject currentPlayer) {
		GameObject randomSpawnPoint = spawnPoints[Random.Range (0, spawnPoints.Count)];
		if (randomSpawnPoint.GetComponent<respawnCar>().isFree == true) {
			Instantiate (currentPlayer, randomSpawnPoint.transform.position, randomSpawnPoint.transform.rotation);
		} else {
			respawnPlayer(currentPlayer);
		}
	}
}
