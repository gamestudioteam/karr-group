﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {
	

	void OnTriggerEnter(Collider other){
		
		if (other.gameObject.name == "Tank") {
			CarHP.tankHP = CarHP.tankHP + 50;
			gameObject.SetActive(false);
			StartCoroutine(waitForHealthRespawn());
			if(CarHP.tankHP > 100){
				CarHP.tankHP = 100;
			}
		}
		if (other.gameObject.name == "Rogue") {
			CarHP.rogueHP = CarHP.rogueHP + 50;
			gameObject.SetActive(false);
			StartCoroutine(waitForHealthRespawn());
			if(CarHP.rogueHP > 100){
				CarHP.rogueHP = 100;
			}
		}
		if (other.gameObject.name == "Sniper") {
			CarHP.sniperHP = CarHP.sniperHP + 50;
			gameObject.SetActive(false);
			StartCoroutine(waitForHealthRespawn());
			if(CarHP.sniperHP > 100){
				CarHP.sniperHP = 100;
			}
		}
		if (other.gameObject.name == "Fighter") {
			CarHP.fighterHP = CarHP.fighterHP + 50;
			gameObject.SetActive(false);
			StartCoroutine(waitForHealthRespawn());
			if(CarHP.fighterHP > 100){
				CarHP.fighterHP = 100;
			}
		}
	}
	IEnumerator waitForHealthRespawn(){
		yield return new WaitForSeconds (20);
		gameObject.SetActive (true);
	}
}
