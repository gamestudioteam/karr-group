﻿using UnityEngine;
using System.Collections;

public class carCollision : MonoBehaviour {
	
	public Vector3 v;
	public static int totalVelocity;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		v = this.GetComponent<Rigidbody> ().velocity;


		totalVelocity = Mathf.RoundToInt(v.magnitude);


	}
	void OnCollisionEnter(Collision other){

		if (other.collider.gameObject.name == "Tank") {
			CarHP.tankHP = CarHP.tankHP - totalVelocity;

		}
		if (other.collider.gameObject.name == "Rogue") {
			CarHP.rogueHP = CarHP.rogueHP - totalVelocity;

		}
		if (other.collider.gameObject.name == "Sniper") {
			CarHP.sniperHP = CarHP.sniperHP - totalVelocity;

		}
		if (other.collider.gameObject.name == "Fighter") {
			CarHP.fighterHP = CarHP.fighterHP - totalVelocity;
		}
	}
}
