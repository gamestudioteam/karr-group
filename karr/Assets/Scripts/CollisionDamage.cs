﻿using UnityEngine;
using System.Collections;

public class CollisionDamage : MonoBehaviour {

	private GameObject control;
	private PlayerControllerScript cs;
	private GameObject colControl;
	private PlayerControllerScript ccs;
	// Use this for initialization
	void Start () {
		if (this.tag == "Player 1") {
			control = GameObject.FindGameObjectWithTag ("Controller 1");
		}
		else if (this.tag == "Player 2") {
			control = GameObject.FindGameObjectWithTag ("Controller 2");
		}
		else if (this.tag == "Player 3") {
			control = GameObject.FindGameObjectWithTag ("Controller 3");
		}
		else if (this.tag == "Player 4") {
			control = GameObject.FindGameObjectWithTag ("Controller 4");
		}
		cs = control.gameObject.GetComponent<PlayerControllerScript> ();
		//Debug.Log(cs.gameObject.name);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.tag == "Player 1") 
		{
			//Debug.Log(col.gameObject.name);
			colControl = GameObject.FindGameObjectWithTag ("Controller 1");
			ccs = colControl.gameObject.GetComponent<PlayerControllerScript> ();


			ccs.CarGotHitCar(cs.GetCarMelleDamage(), cs.currentSpeed);
		}
		else if (this.tag == "Player 2") {
			//Debug.Log(col.gameObject.name);
			colControl = GameObject.FindGameObjectWithTag ("Controller 2");
			ccs = colControl.gameObject.GetComponent<PlayerControllerScript> ();
			ccs.CarGotHitCar(cs.GetCarMelleDamage(), cs.currentSpeed);
		}
		else if (this.tag == "Player 3") {
			//Debug.Log(col.gameObject.name);
			colControl = GameObject.FindGameObjectWithTag ("Controller 3");
			ccs = colControl.gameObject.GetComponent<PlayerControllerScript> ();
			ccs.CarGotHitCar(cs.GetCarMelleDamage(), cs.currentSpeed);
		}
		else if (this.tag == "Player 4") {
			//Debug.Log(col.gameObject.name);
			colControl = GameObject.FindGameObjectWithTag ("Controller 4");
			ccs = colControl.gameObject.GetComponent<PlayerControllerScript> ();
			ccs.CarGotHitCar(cs.GetCarMelleDamage(), cs.currentSpeed);
		}

	}
}
