﻿using UnityEngine;
using System.Collections;

public class CarHP : MonoBehaviour {

	public static int sniperHP = 100;
	public static int fighterHP = 100;
	public static int tankHP = 100;
	public static int rogueHP = 100;

	void start(){
		rogueHP = 100;
		fighterHP = 100;
		sniperHP = 100;
		tankHP = 100;
	}

	void Update(){
		if (sniperHP <= 0 && this.gameObject.name == "Sniper") {
			Destroy (this.gameObject);
		}
		if (tankHP <= 0 && this.gameObject.name == "Tank") {
			Destroy (this.gameObject);
		}
		if (fighterHP <= 0 && this.gameObject.name == "Fighter") {
			Destroy (this.gameObject);
		}
		if (rogueHP <= 0 && this.gameObject.name == "Rogue") {
			Destroy (this.gameObject);
		}
	}
}
