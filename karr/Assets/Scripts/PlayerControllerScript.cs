﻿using UnityEngine;
using System.Collections;

public class PlayerControllerScript : MonoBehaviour {

	//Car State
	private bool isInstantiated;
	public bool isCarActive = true;

	//Reference for Game Controller
	private GameObject gC;
	private GameController gameController;

	//Reference for Camera
	private GameObject cam;

	//Reference for HUD
	private GameObject hud;

	//Current Car
	private GameObject currentCar;
	
	//CAR STATS
	private GameObject currentCarStats;
	private CarStats stats;
	
	private float armorBulletModifier;
	private float armorMeleeModifier;
	
	private float damageBulletModifier;
	private float damageMeleeModifier;
	
	private float hpTotal;
	private float specialCooldownCap;
	
	private float rateOfFire;
	private float rangeOfBullets;

	//tIMERS
	private int currentSpawnTimer;

	//PLAYER STATS
	public float currentHp;
	public float currentSpeed;
	
	public int currentPlayer;
	public string currentCarType;

	public int lives;

	void Awake() {
		DontDestroyOnLoad(this);
	}
	// Use this for initialization
	void Start () {

		gC = GameObject.FindGameObjectWithTag ("Game Controller");
		gameController = gC.GetComponent<GameController> ();

		gameController.SetIsOnMatch (true);
		isInstantiated = true;

		//for testing
		armorBulletModifier = 20;
		armorMeleeModifier = 25;
		
		damageBulletModifier = 50;
		damageMeleeModifier = 50;
		
		hpTotal = 100;
		specialCooldownCap = 100;
		
		rateOfFire = 100;
		rangeOfBullets = 100;

		lives = 1;
		
		currentHp = hpTotal;
		
	}
	
	// Update is called once per frame
	void Update () {

		if (gameController.GetIsOnMatch ()) {

				if (isInstantiated) {
					
					SetCurrentCar();
					if(this.currentCar != null)
					{	
						SetCurrentSpeed ();
					

						if (this.currentHp <= 0) {
							this.CarDeath ();
						}
					
						if (Input.GetMouseButtonDown (0)) {
							CarPrimaryWeaponShoot ();
						}
					}
				} 
				
				else {
					setNewSpawnTimer ();
				}
		
		}

		
	}

	public float GetCarMelleDamage()
	{
		return this.damageMeleeModifier;
	}
	
	public void SetCurrentCarStats(GameObject ccs) {
		currentCarStats = ccs;
		stats = currentCarStats.GetComponent<CarStats> ();
		SetPlayerCarStats ();
	}
	
	public void SetPlayerCarStats () {
		armorBulletModifier = stats.GetCarArmorBulletModifier ();
		armorMeleeModifier = stats.GetCarArmorMeleeModifier ();
		
		damageBulletModifier = stats.GetCarDamageBulletModifier ();
		damageMeleeModifier = stats.GetCarDamageMeleeModifier ();
		
		hpTotal = stats.GetCarHpTotal ();
		specialCooldownCap = stats.GetCarSpecialCooldownCap ();
		
		rateOfFire = stats.GetCarRateOfFire ();
		rangeOfBullets = stats.GetCarRangeOfBullets ();
	}
	
	public void SetCurrentCar() {
		if (this.currentPlayer == 1) {
			currentCar = GameObject.FindWithTag ("Player 1");
		}
		else if (this.currentPlayer == 2) {
			currentCar = GameObject.FindWithTag ("Player 2");
		}
		else if (this.currentPlayer == 3) {
			currentCar = GameObject.FindWithTag ("Player 3");
		}
		else if (this.currentPlayer == 4) {
			currentCar = GameObject.FindWithTag ("Player 4");
		}
	}
	
	public void SetCurrentSpeed() {
		Rigidbody carRigidBody;
		
		carRigidBody = currentCar.gameObject.GetComponent<Rigidbody> ();
		currentSpeed = carRigidBody.velocity.magnitude;
	}
	
	
	public void CarPrimaryWeaponShoot() {
		
		RaycastHit hit;
		Ray gunRay;
		
		Transform aim = currentCar.transform.Find ("Aim");
		
		gunRay	= new Ray (aim.gameObject.transform.position, aim.forward);
		
		Debug.DrawRay(aim.position, aim.forward * rangeOfBullets, Color.red);
		
		if (Physics.Raycast (gunRay, out hit, rangeOfBullets)) {
			Debug.Log ("shot");
			if (hit.collider.gameObject.tag == "destructable") 
			{
				Debug.Log ("SHOT");
			}
			else if (hit.collider.gameObject.tag == "Player 1") 
			{
				GameObject hitObject = GameObject.FindGameObjectWithTag("Controller 1");
				PlayerControllerScript hitController = hitObject.GetComponent<PlayerControllerScript>();
				Debug.Log(hitController.currentPlayer);
				hitController.CarGotHitBullet(this.damageBulletModifier);
	
				
			}
			else if (hit.collider.gameObject.tag == "Player 2") 
			{
				GameObject hitObject = GameObject.FindGameObjectWithTag("Controller 2");
				PlayerControllerScript hitController = hitObject.GetComponent<PlayerControllerScript>();
				hitController.CarGotHitBullet(this.damageBulletModifier);
				
			}
			else if (hit.collider.gameObject.tag == "Player 3") 
			{
				GameObject hitObject = GameObject.FindGameObjectWithTag("Controller 3");
				PlayerControllerScript hitController = hitObject.GetComponent<PlayerControllerScript>();
				hitController.CarGotHitBullet(this.damageBulletModifier);
				
			}
			else if (hit.collider.gameObject.tag == "Player 4") 
			{
				GameObject hitObject = GameObject.FindGameObjectWithTag("Controller 4");
				PlayerControllerScript hitController = hitObject.GetComponent<PlayerControllerScript>();
				hitController.CarGotHitBullet(this.damageBulletModifier);
				
			}
		}
		
	}
	
	public void CarGotHitBullet ( float attackerDamage )
	{
		this.currentHp -= attackerDamage - this.armorBulletModifier;
	}

	public void CarGotHitCar ( float attackerDamage, float attackerSpeed )
	{
		//Debug.Log(this.tag);
		//Debug.Log((attackerDamage * ( attackerSpeed)) - this.damageMeleeModifier);
		this.currentHp -= (attackerDamage * ( attackerSpeed)) - this.damageMeleeModifier;
		//Debug.Log (this.currentHp);
	}
	
	public void CarDeath ()
	{
		Destroy(currentCar);
		lives--;
		if (lives > 0) {
			currentHp = hpTotal;
			setNewSpawnTimer ();
		} 
		else {
			cam = GameObject.FindGameObjectWithTag ("Camera " + currentPlayer);
			hud = GameObject.FindGameObjectWithTag ("HUD " + currentPlayer);
			cam.SetActive(false);
			hud.SetActive(false);
			isCarActive = false;

		}
	}

	void setNewSpawnTimer () {
		//Debug.Log ("back to work" + this.tag);
		currentSpawnTimer = 1;
		countdownSpawnTimer ();
		Invoke ("countdownSpawnTimer", 1f);
	}

	void countdownSpawnTimer () {
		currentSpawnTimer--;
		Invoke ("countdownSpawnTimer", 1f);
		if (currentSpawnTimer == 0) {
			gameController.respawnPlayerTaskManager(stats.carPrefab, currentPlayer);
			Debug.Log (currentPlayer +" sent to respawnPlayer");
			SetCurrentCar ();
			
			cam = GameObject.FindGameObjectWithTag ("Camera " + currentPlayer);
			isInstantiated = true;

		}
	}

	
	// TEST METHIODS
	
	public void PrintStats () {
		Debug.Log (armorBulletModifier);
		Debug.Log (armorMeleeModifier);
		
		Debug.Log (damageBulletModifier);
		Debug.Log (damageMeleeModifier);
		
		Debug.Log (hpTotal);
		Debug.Log (specialCooldownCap);
		
		Debug.Log (rateOfFire);
		Debug.Log (rangeOfBullets);
	}
}
