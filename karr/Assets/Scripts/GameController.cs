﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	//Game State
	private bool isOnMatch;
	private bool respawnCall;

	public List<GameObject> spawnPoints;

	public GameObject player1;
	public GameObject player2;
	public GameObject player3;
	public GameObject player4;

	public PlayerControllerScript control1;
	public PlayerControllerScript control2;
	public PlayerControllerScript control3;
	public PlayerControllerScript control4;

	public int winner;

	void Awake() {
		DontDestroyOnLoad(this);
	}

	// Use this for initialization
	void Start () {
		winner = 0;
		isOnMatch = false;
		respawnCall = false;
		spawnPoints [0] = null;
		spawnPoints [1] = null;
		spawnPoints [2] = null;
		spawnPoints [3] = null;
		spawnPoints [4] = null;
		spawnPoints [5] = null;


	}
	
	// Update is called once per frame
	void Update () {

		if (isOnMatch == true && spawnPoints.Count > 0 && spawnPoints [0] == null) {
			StartMatch();
		}

		if (isOnMatch == true) {
			CheckWinner();
		}


	}

	void CheckWinner() 
	{
		bool active1 = control1.isCarActive;
		bool active2 = control2.isCarActive;
		bool active3 = control3.isCarActive;
		bool active4 = control4.isCarActive;

		if (active1 && !active2 && !active3 && !active4) {
			winner = 1;
			isOnMatch = false;
			Application.LoadLevel("GameOver");
		}
		else if (!active1 && active2 && !active3 && !active4) {
			winner = 2;
			isOnMatch = false;
			Application.LoadLevel("GameOver");
		}
		else if (!active1 && !active2 && active3 && !active4) {
			winner = 3;
			isOnMatch = false;
			Application.LoadLevel("GameOver");
		}
		else if (!active1 && !active2 && !active3 && active4) {
			winner = 4;
			isOnMatch = false;
			Application.LoadLevel("GameOver");
		}
	}

	public void SetIsOnMatch(bool b)
	{
		isOnMatch = b;
	}

	public bool GetIsOnMatch() {
		return isOnMatch;
	}

	void StartMatch() {
		spawnPoints [0] = GameObject.FindWithTag ("Respawn 1");
		spawnPoints [1] = GameObject.FindWithTag ("Respawn 2");
		spawnPoints [2] = GameObject.FindWithTag ("Respawn 3");
		spawnPoints [3] = GameObject.FindWithTag ("Respawn 4");
		spawnPoints [4] = GameObject.FindWithTag ("Respawn 5");
		spawnPoints [5] = GameObject.FindWithTag ("Respawn 6");
	}

	public void respawnPlayerTaskManager (GameObject currentPlayer, int playerNumber) {

		Debug.Log (playerNumber + " " + respawnCall);

		if (!respawnCall) {
			int index = Random.Range (0, spawnPoints.Count - 1);

			respawnCall = true;

			GameObject randomSpawnPoint = spawnPoints [index];
			RespawnController rC = randomSpawnPoint.GetComponent<RespawnController> ();

			spawnPoints.Remove(randomSpawnPoint);

			Debug.Log (playerNumber + " " + randomSpawnPoint.tag + " " + rC.isFree);

			if (rC.GetIsFree () == true) {
				respawnPlayer (currentPlayer, playerNumber, randomSpawnPoint);
				Debug.Log ("Did " + playerNumber);
				respawnCall = false;
			} 

			else {

				respawnPlayerTaskManager (currentPlayer, playerNumber);
			}

		}

		else {
			respawnPlayerTaskManager (currentPlayer, playerNumber);
		}
	}

	void respawnPlayer (GameObject currentPlayer, int playerNumber, GameObject randomSpawnPoint) {
			GameObject c;
			
			RespawnController rC = randomSpawnPoint.GetComponent<RespawnController> ();
			c = Instantiate (currentPlayer, randomSpawnPoint.transform.position, randomSpawnPoint.transform.rotation) as GameObject;
			Debug.Log ("Spwaning Player " + playerNumber);
		      //randomSpawnPoint.GetComponent<BoxCollider > ().enabled = false;
			//rC.isFree = false;
			
			//randomSpawnPoint.GetComponent<BoxCollider > ().enabled = true;
			//Debug.Log("just used respawn status " +  rC.isFree);
			c.tag = "Player " + playerNumber;
	}

}	