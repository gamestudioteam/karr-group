﻿using UnityEngine;
using System.Collections;

public class CarStats : MonoBehaviour {

	public GameObject carPrefab;

	public float armorBulletModifier;
	public float armorMeleeModifier;

	public float damageBulletModifier;
	public float damageMeleeModifier;

	public float hpTotal;
	public float specialCooldownCap;

	public float rateOfFire;
	public float rangeOfBullets;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public float GetCarArmorBulletModifier() {
		return armorBulletModifier;
	}

	public float GetCarArmorMeleeModifier() {
		return armorMeleeModifier;
	}

	public float GetCarDamageBulletModifier() {
		return damageBulletModifier;
	}
	
	public float GetCarDamageMeleeModifier() {
		return damageMeleeModifier;
	}

	public float GetCarHpTotal() {
		return hpTotal;
	}
	
	public float GetCarSpecialCooldownCap() {
		return specialCooldownCap;
	}

	public float GetCarRateOfFire() {
		return rateOfFire;
	}
	
	public float GetCarRangeOfBullets() {
		return rangeOfBullets;
	}
}
