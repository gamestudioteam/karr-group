﻿using UnityEngine;
using System.Collections;

public class RespawnController : MonoBehaviour {

	public bool isFree;

	private GameObject gC;
	private GameController gameController;

	void Start () {
		isFree = true;

		gC = GameObject.FindGameObjectWithTag ("Game Controller");
		gameController = gC.GetComponent<GameController> ();
	}

	void update () {
		
	}
	
	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == "Player 1" || other.gameObject.tag == "Player 2" || other.gameObject.tag == "Player 3" || other.gameObject.tag == "Player 4" || other.gameObject.tag == "destructable") {
			isFree = false;		
		}
	}
	
	void OnTriggerStay (Collider other) {
		if (other.gameObject.tag == "Player 1" || other.gameObject.tag == "Player 2" || other.gameObject.tag == "Player 3" || other.gameObject.tag == "Player 4" || other.gameObject.tag == "destructable") {
			isFree = false;
		}
	}
	
	void OnTriggerExit (Collider other) {
		if (other.gameObject.tag == "Player 1" || other.gameObject.tag == "Player 2" || other.gameObject.tag == "Player 3" || other.gameObject.tag == "Player 4" || other.gameObject.tag == "destructable") {
			isFree = true;
			gameController.spawnPoints.Add(this.gameObject);
				
		}
	}

	public bool GetIsFree() {
		return isFree;
	}
}
