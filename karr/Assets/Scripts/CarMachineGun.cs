﻿using UnityEngine;
using System.Collections;

public class CarMachineGun : MonoBehaviour {
	
	//private AudioSource audioS;
	public GameObject aim;                  
	public float weaponRange = 50;
	public int weaponDamage = 20;
	//public AudioClip gunClip;
	//	private GameObject tankParticle;
	//	private GameObject sniperParticle;
	//	private GameObject rogueParticle;
	//	private GameObject fighterParticle;
	
	// Use this for initialization
	void Start () {
		//audioS = new AudioSource();
		//audioS.clip = gunClip;
		
	}                    
	
	// Update is called once per frame
	
	void Update () {
		
		Debug.DrawRay(aim.gameObject.transform.position, aim.gameObject.transform.forward * weaponRange, Color.red);
		
		if (Input.GetMouseButtonDown (0)) {
			
			RaycastHit hit;
			Ray gunRay = new Ray (aim.gameObject.transform.position, aim.gameObject.transform.forward);
			
			//audioS.Play();
			
			//Debug.Log (audioS.clip);
			//Debug.Log ("pew");
			
			if (Physics.Raycast (gunRay, out hit, weaponRange)) {
				print (hit.collider.gameObject.name);
				
//				if (hit.collider.gameObject.tag == "destructable" || hit.collider.gameObject.tag == "Player 1" || hit.collider.gameObject.tag == "Player 2" || hit.collider.gameObject.tag == "Player 3" || hit.collider.gameObject.tag == "Player 4") {
//					Debug.Log ("SHOT");
//				}
//				if (hit.collider.gameObject.name == "Tank") {
//					CarHP.tankHP = CarHP.tankHP - weaponDamage;
//					print (CarHP.tankHP);
//				}
//				if (hit.collider.gameObject.name == "Fighter") {
//					CarHP.fighterHP = CarHP.fighterHP - weaponDamage;
//					print (CarHP.tankHP);
//				}
//				if (hit.collider.gameObject.name == "Rogue") {
//					CarHP.rogueHP = CarHP.rogueHP - weaponDamage;
//					print (CarHP.tankHP);
//				}
//				if (hit.collider.gameObject.name == "Sniper") {
//					CarHP.sniperHP = CarHP.sniperHP - weaponDamage;
//					print (CarHP.tankHP);
//				}
				if(hit.collider.gameObject.tag == "Vehicle"){
					hit.collider.gameObject.GetComponent<ParticleSystem>().Play();
				}
			}
		}
	}
}