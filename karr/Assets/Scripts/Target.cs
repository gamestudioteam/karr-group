﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

	public int cameraTarget;
	private GameObject Player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Player = GameObject.FindGameObjectWithTag ("Player " + cameraTarget);
		if (Player != null) {
			this.gameObject.transform.position = Player.gameObject.transform.position;
			this.gameObject.transform.rotation = Player.gameObject.transform.rotation;
		}
	}
}
