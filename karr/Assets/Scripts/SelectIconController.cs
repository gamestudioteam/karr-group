﻿using UnityEngine;
using System.Collections;

public class SelectIconController : MonoBehaviour {

	//Reference for Game Controller
	private GameObject gC;
	private GameController gameController;

	public float pos;
	public float originalX;
	public int cursorPlayer;
	private GameObject car;
	private GameObject playerController;
	public PlayerControllerScript controllerScript;

	private bool Delay;
	
	private float delayTime;
	private float analogSensivity;

	// Use this for initialization
	void Start () {

		Delay = false;
		delayTime = 0.3f;
		analogSensivity = 0.5f;

		gC = GameObject.FindGameObjectWithTag ("Game Controller");
		gameController = gC.GetComponent<GameController> ();

		pos = 0;
		originalX = this.gameObject.transform.position.x;
		Debug.Log ("Controller " + this.cursorPlayer);
		playerController = GameObject.FindGameObjectWithTag("Controller " + this.cursorPlayer);
		controllerScript = playerController.GetComponent<PlayerControllerScript>();
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("P2 horziontal axis: " + Input.GetAxis ("P2 Horizontal"));
		if (!Delay) {
			if (this.tag == "Cursor 1") {
				if (Input.GetAxis ("P1 Horizontal") > analogSensivity || Input.GetButtonDown("AP1 Right")) {
					Delay = true;
					Invoke ("stopDelay", delayTime);
					pos++;
					pos = pos % 4;
					if (pos > 0) {
						this.transform.Translate (1.72f, 0, 0);
					} else {
						this.transform.Translate (-5.16f, 0, 0);
					//	Vector3 offset = new Vector3 (4.0f, 0, 0);
					//	this.transform.Translate (this.gameObject.transform.position.x + 4, 0, 0);
					//	this.transform.Translate (4, 0, 0);
					//} else {
					//	this.transform.Translate (this.gameObject.transform.position.x + -8, 0, 0);
					//	this.transform.Translate (-12, 0, 0);
					}
				}

				if (Input.GetAxis ("P1 Horizontal") < -analogSensivity || Input.GetButtonDown("AP1 Left")) {
					Delay = true;
					Invoke ("stopDelay", delayTime);
					pos--;
					if (pos < 0) {
						pos = 3;
					}

					if (pos < 3) {
						this.transform.Translate (-1.72f, 0, 0);
					} else {
						this.transform.Translate (5.16f, 0, 0);
					}
				}

				if (Input.GetKeyDown (KeyCode.X)) {
					car = GameObject.FindGameObjectWithTag ("Car " + pos);
					controllerScript.SetCurrentCarStats (car);

				}


			} 
			if (this.tag == "Cursor 2") {
				if (Input.GetAxis ("P1 Horizontal") > analogSensivity || Input.GetButtonDown("AP2 Right")) {
					Delay = true;
					Invoke ("stopDelay", delayTime);
					pos++;
					pos = pos % 4;
					if (pos > 0) {
						this.transform.Translate (1.72f, 0, 0);
					} else {
						this.transform.Translate (-5.16f, 0, 0);
					}
				}
				
				if (Input.GetAxis ("P1 Horizontal") < -analogSensivity || Input.GetButtonDown("AP2 Left")) {
					Delay = true;
					Invoke ("stopDelay", delayTime);
					pos--;
					if (pos < 0) {
						pos = 3;
					}
					
					if (pos < 3) {
						this.transform.Translate (-1.72f, 0, 0);
					} else {
						this.transform.Translate (5.16f, 0, 0);
					}
				}
				
				if (Input.GetKeyDown (KeyCode.C)) {
					car = GameObject.FindGameObjectWithTag ("Car " + pos);
					controllerScript.SetCurrentCarStats (car);

				}
			
			} 
			if (this.tag == "Cursor 3") {

				if (Input.GetAxis ("P3 Horizontal") > analogSensivity || Input.GetButtonDown("AP3 Right")) {
					Delay = true;
					Invoke ("stopDelay", delayTime);
					pos++;
					pos = pos % 4;
					if (pos > 0) {
						this.transform.Translate (1.72f, 0, 0);
					} else {
						this.transform.Translate (-5.16f, 0, 0);
					}
				}
				
				if (Input.GetAxis ("P3 Horizontal") < -analogSensivity || Input.GetButtonDown("AP3 Left")) {
					Delay = true;
					Invoke ("stopDelay", delayTime);
					pos--;
					if (pos < 0) {
						pos = 3;
					}
					
					if (pos < 3) {
						this.transform.Translate (-1.72f, 0, 0);
					} else {
						this.transform.Translate (5.16f, 0, 0);
					}
				}
				
				if (Input.GetKeyDown (KeyCode.V)) {
					car = GameObject.FindGameObjectWithTag ("Car " + pos);
					controllerScript.SetCurrentCarStats (car);
				}
			}
			if (this.tag == "Cursor 4") {

				if (Input.GetAxis ("P4 Horizontal") > analogSensivity || Input.GetButtonDown("AP4 Right")) {
					Delay = true;
					Invoke ("stopDelay", delayTime);
					pos++;
					pos = pos % 4;
					if (pos > 0) {
						this.transform.Translate (1.72f, 0, 0);
					} else {
						this.transform.Translate (-5.16f, 0, 0);
					}
				}
				
				if (Input.GetAxis ("P4 Horizontal") < -analogSensivity || Input.GetButtonDown("AP4 Left")) {
					Delay = true;
					Invoke ("stopDelay", delayTime);
					pos--;
					if (pos < 0) {
						pos = 3;
					}
					
					if (pos < 3) {
						this.transform.Translate (-1.72f, 0, 0);
					} else {
						this.transform.Translate (5.16f, 0, 0);
					}
				}
				
				if (Input.GetKeyDown (KeyCode.B)) {
					if (pos == 0) {
					car = GameObject.FindGameObjectWithTag ("Car " + pos);
					GameObject.Find("P1CarPhoto");
					controllerScript.SetCurrentCarStats (car);
					}
				}
			}

			if (Input.GetKeyDown (KeyCode.Z)) {
				gameController.SetIsOnMatch (true);
				Application.LoadLevel (2);
			}

		}

	}

	void stopDelay()
	{
		Delay = false;
	}	
}


