﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AlternateWeapon : MonoBehaviour {
	public GameObject AltWeapon;	
	public GameObject Missile;
	public GameObject SpecialAbility3;
	public int shot;
	//public Animation shieldAnimation = SpecialAbility3.GetComponent.Animator;
	//public bool animationIsPlaying = shieldAnimation.isPlaying;
	public int currentCooldownTimer = 60;

	// SpecialAbility3.GetComponent<Animation>(SpecialAbility3).Play();
	// Use this for initialization

	void setNewCooldownTimer () {
		currentCooldownTimer = 60;
		InvokeRepeating ("countdownCooldownTimer", 1f, 1f);
	}
	
	void countdownCooldownTimer () {
		print (currentCooldownTimer);
		currentCooldownTimer--;
		print (currentCooldownTimer);
		if (currentCooldownTimer == 0) {
	//		print ("done");
			shot = 13;
			SpecialAbility3.SetActive (false);
			CancelInvoke ("countdownCooldownTimer");
		}
	}

	void Start () {
		SpecialAbility3.SetActive (false);
		shot = 13;
	}
	


	void Update () {

		if (currentCooldownTimer == 0 && SpecialAbility3.activeInHierarchy == false){
			if (Input.GetKeyDown (KeyCode.X)){
				SpecialAbility3.SetActive (true);
			}
		}
		if (Input.GetKeyDown (KeyCode.X) && SpecialAbility3.activeInHierarchy == true){
			Instantiate (Missile, transform.position, transform.rotation);
			shot = shot - 1;
		//	print ("fire");
			if (shot == 0) {
				print ("respawn");
				SpecialAbility3.SetActive(false);
				setNewCooldownTimer();
			}
		}
	}
}
