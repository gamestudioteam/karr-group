﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class missleForce : MonoBehaviour {
	private int timer;
	// Use this for initialization
	void Start () {
		timer = 0;

	} 
	
	// Update is called once per frame
	void Update () {
		InvokeRepeating ("seekTarget", 0.5f, 0.5f);
		this.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
		this.GetComponent<Rigidbody>().AddForce(this.transform.forward*0.0004f   );
	}

	void seekTarget() {
		timer++;
		if (timer >= 10) {
	
		}
	}

	GameObject GetNearestTarget()
	{
		//so lets say you want the closest target from a array (in this case all Gameobjects with Tag "enemy") and let's assume this script right now is on the player (or the object with which the distance has to be calculated)
		return GameObject.FindGameObjectsWithTag("Enemy").Aggregate((o1, o2) => Vector3.Distance(o1.transform.position, this.transform.position) > Vector3.Distance(o2.transform.position, this.transform.position) ? o2 : o1);
	}

	void OnCollisionEnter (Collision col){
	
		Destroy (this.gameObject);
	}
}