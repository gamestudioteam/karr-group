using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
	[RequireComponent(typeof (CarController))]
	public class CarUserControl : MonoBehaviour
	{
		private CarController m_Car; // the car controller we want to use
		private int playerNumber;
		
		
		private void Awake()
		{
			// get the car controller
			m_Car = GetComponent<CarController>();
			playerNumber = 0;
			
		}
		
		
		private void FixedUpdate()
		{
			if (this.tag == "Player 1") {
				playerNumber = 1;
			}
			else if (this.tag == "Player 2") {
				playerNumber = 2;
			}
			else if (this.tag == "Player 3") {
				playerNumber = 3;
			}
			else if (this.tag == "Player 4") {
				playerNumber = 4;
			}
			// pass the input to the car!
			if (playerNumber == 1) {
				float h;
				float v;
				h = CrossPlatformInputManager.GetAxis("P1 Horizontal");
				v = CrossPlatformInputManager.GetAxis("P1 Vertical");
				if(Input.GetButton("AP1 Right"))
				{
					h = 1;
				}
				if(Input.GetButton("AP1 Left"))
				{
					h = -1;
				}
				if(Input.GetButton("AP1 Down"))
				{
					v = -1;
				}
				if(Input.GetButton("AP1 Up"))
				{
					v = 1;
				}
				float handbrake = CrossPlatformInputManager.GetAxis("Jump");
				m_Car.Move(h, v, v, handbrake);
				
			}
			if (playerNumber == 2) {
				float h;
				float v;
				h = CrossPlatformInputManager.GetAxis("P2 Horizontal");
				v = CrossPlatformInputManager.GetAxis("P2 Vertical");
				if(Input.GetButton("AP2 Right"))
				{
					h = 1;
				}
				if(Input.GetButton("AP2 Left"))
				{
					h = -1;
				}
				if(Input.GetButton("AP2 Down"))
				{
					v = -1;
				}
				if(Input.GetButton("AP2 Up"))
				{
					v = 1;
				}
				
				float handbrake = CrossPlatformInputManager.GetAxis("Jump");
				m_Car.Move(h, v, v, handbrake);
				
			}
			if (playerNumber == 3) {
				float h;
				float v;
				h = CrossPlatformInputManager.GetAxis("P3 Horizontal");
				v = CrossPlatformInputManager.GetAxis("P3 Vertical");
				if(Input.GetButton("AP3 Right"))
				{
					h = 1;
				}
				if(Input.GetButton("AP3 Left"))
				{
					h = -1;
				}
				if(Input.GetButton("AP3 Down"))
				{
					v = -1;
				}
				if(Input.GetButton("AP3 Up"))
				{
					v = 1;
				}
				float handbrake = CrossPlatformInputManager.GetAxis("Jump");
				m_Car.Move(h, v, v, handbrake);

				
			}
			if (playerNumber == 4) {
				float h;
				float v;
				h = CrossPlatformInputManager.GetAxis("P4 Horizontal");
				v = CrossPlatformInputManager.GetAxis("P4 Vertical");
				if(Input.GetButton("AP4 Right"))
				{
					h = 1;
				}
				if(Input.GetButton("AP4 Left"))
				{
					h = -1;
				}
				if(Input.GetButton("AP4 Down"))
				{
					v = -1;
				}
				if(Input.GetButton("AP4 Up"))
				{
					v = 1;
				}
				float handbrake = CrossPlatformInputManager.GetAxis("Jump");
				m_Car.Move(h, v, v, handbrake);
				
			}
			//            float h = CrossPlatformInputManager.GetAxis("Horizontal");
			//            float v = CrossPlatformInputManager.GetAxis("Vertical");
			#if !MOBILE_INPUT
			//            float handbrake = CrossPlatformInputManager.GetAxis("Jump");
			//            m_Car.Move(h, v, v, handbrake);
			#else
			m_Car.Move(h, v, v, 0f);
			#endif
		}
		
		//		if(playerNum==1)
		//		{
		//			value=Input.GetAxis("Player1 left/right");
		//		}
		//		else if(playerNum==2)
		//		{
		//			value=Input.GetAxis("Player2 left/right");
		//		}
	}
}
